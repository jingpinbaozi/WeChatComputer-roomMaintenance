const cloud = require('wx-server-sdk')
//这里最好也初始化一下你的云开发环境
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
//操作excel用的类库
const xlsx = require('node-xlsx');

// 云函数入口函数
exports.main = async(event, context) => {
  try {
    // let getdata = await cloud.database().collection('c_apply').get();
    let applydata = event.exportData;
    //1,定义excel表格名
    // let dataCVS = '报修工单报表.xlsx'
    let dataCVS = event.fileName;
    //2，定义存储数据的
    let alldata = [];
    let row = ['时间', '机房', '报修人姓名', '报修人电话', '机房','机器号', '问题描述', '问题等级', '处理状态','处理人姓名','处理人手机号','维修反馈']; //表属性
    alldata.push(row);

    for (let key in applydata) {
      let arr = [];
      arr.push(applydata[key].createTime);
      arr.push(applydata[key].floor);
      arr.push(applydata[key].name);
      arr.push(applydata[key].phone);
      arr.push(applydata[key].floor);
      arr.push(applydata[key].dorm);
      arr.push(applydata[key].desc);
      arr.push(applydata[key].level);
      arr.push(applydata[key].status);
      arr.push(applydata[key].admin_name);
      arr.push(applydata[key].admin_phone);
      arr.push(applydata[key].admin_tallText);
      alldata.push(arr)
    }
    //3，把数据保存到excel里
    var buffer = await xlsx.build([{
      name: "mySheetName",
      data: alldata
    }]);
    //4，把excel文件保存到云存储里
    return await cloud.uploadFile({
      cloudPath: dataCVS,
      fileContent: buffer, //excel二进制文件
    })

  } catch (e) {
    console.error(e)
    return e
  }
}