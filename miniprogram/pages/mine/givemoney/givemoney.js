// pages/home/givemoney/givemoney.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    principal_money:1,
    kdtype: [{ la: '1元', price: 1 }, { la: '5元', price: 5 }, { la: '10元', price: 10 }, { la: '20元', price: 20 }, { la: '30元', price: 30 }, { la: '50元', price: 50 }],
    flag:0,
    index: 0,
    userinfo:{}
  },
  getData(){
    let userinfo = wx.getStorageSync('userInfo');
    this.setData({
      userinfo
    })
  },
  //选择金额
  change(e){
    this.setData({
      principal_money: this.data.kdtype[e.currentTarget.dataset.index].price,
      flag: e.currentTarget.dataset.index,
    })

  },
  givemoney(e){
    this.setData({
      principal_money:e.detail.value,
    })
  },
  //支付方法
 goPay(){
  let that = this;
  const orderid = Math.floor(Math.random() * 9999) * Date.now() + ""; //String 商户订单号 确保唯一
  // wx.setStorageSync('"orderid"',orderid);
  // 小程序代码
  wx.cloud.callFunction({
    name: 'pay',
    data: {
      orderid:orderid,
      body:"机房报修-打赏",
      totalFee:this.data.principal_money * 100,
    },
    success: res => {
      const payment = res.result.payment
      wx.requestPayment({
        ...payment,
        success (res) {
          console.log('支付成功', res)
          wx.showModal({
            cancelColor: 'cancelColor',
            title:"我收到你的打赏了~",
            content:"感谢您的打赏，我们会继续努力的！",
            showCancel:false
          })
        },
        fail (err) {
          console.error('支付失败', err)
        }
      })
    },
    fail: console.error,
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getData()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})